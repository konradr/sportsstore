angular.module("sportsStore")
  .controller("sportsStoreCtrl", function ($scope) {
    $scope.data = {
      products: [
        { name: "Produkt #1", description: "To jest produkt 1.", category: "Kategoria 1", price: 200 },
        { name: "Produkt #2", description: "To jest produkt 2.", category: "Kategoria 2", price: 300 },
        { name: "Produkt #3", description: "To jest produkt 3.", category: "Kategoria 3", price: 400 },
        { name: "Produkt #4", description: "To jest produkt 4.", category: "Kategoria 1", price: 500 },
        { name: "Produkt #5", description: "To jest produkt 5.", category: "Kategoria 2", price: 500 },
        { name: "Produkt #6", description: "To jest produkt 6.", category: "Kategoria 1", price: 600 },
        { name: "Produkt #7", description: "To jest produkt 7.", category: "Kategoria 3", price: 700 },
        { name: "Produkt #8", description: "To jest produkt 8.", category: "Kategoria 1", price: 800 }
      ]
    };
  });